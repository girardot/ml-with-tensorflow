# TOD (Tensorflow Object Detection) API

This directory contains some Python programs useful to process data used for the Tensorflow Object Detection API.

Python programs are adapted from https://github.com/TannerGilbert/Tensorflow-Object-Detection-API-Train-Model.

In the present work the "..._tt.py" programs assume a specific tree in order to process automatically the `train` and `test` data :

    ./images/
        |---<project_name>/
        |       |----train/
        |       |      |-----*.jpg
        |       |      |-----*.xml
        |       |
        |       |----test/
        |       |      |-----*.jpg
        |       |      |-----*.xml
        |       |
        |       |----train_labels.csv
        |       |----test_labels.csv
        |
    ./training/
        |---<project_name>/
        |       |---<pre_trained_network>/
        |       |              |
        |       |
        |       |----train.record
        |       |----test.record
        
